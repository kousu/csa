
Toolchain
---------

You need to android SDK installed.
How to do this depends on your system, but look around your package manager if you have one

### TODO: links, Windows, OS X, Arch, Debian, Ubuntu, etc


Building
--------

```
gradle assembleDebug
```

This will give you `build/outputs/apk/*.apk`.


Testing
-------

If you're connected with adb you can install the app with

```
gradle assembleDebug
adb install -g -r build/outputs/apk/app-debug.apk
```

but! some ROMs don't respect the `-g` flag, and for these
you need to manually install it once through the Android Package Installer
so that permissions can be granted. You have to redo this whenever you _update permissions_.
If you don't do this *you will* be trapped in a horrible nether pit of unexplained failures.
That is, without doing this step, you won't even be *prompted* to grant permissions to the app,
it will just fail. And without a slew of permissions the OS simply will not spawn it and won't
bother to explain why.

In normal development, so long as you aren't mucking with permissions, you can just use this shortcut to do both steps:

```
gradle installDebug
```

You can set the output (from `Log.d(tag, message)`) with

```
adb logcat -s ca.kousu.contactsync_jmp.SyncAdapter # or put in your own choice of class here
```

or

```
adb logcat | tee logcat | grep ca.kousu # often this is more useful
```


### Wireless ADB


You can do adb over the LAN too, which should be secure enough so long as you don't trust random keys.
There are a few different ways to arrange this; sometimes there's an option to enable TCP/IP ADB inside
of your phone's Developer Settings and sometimes you can do it from the command line like

```
IP=$(adb shell ip addr show wlan0 | egrep -o "inet ([[:digit:]]+\.){,3}([[:digit:]])+")
PORT=5556
adb tcpip $PORT
```

Once its enabled, connect to it with

```
adb kill-server
adb connect $IP:$PORT
```


Signing
-------

To build a proper release that can go on the Play Store ...

