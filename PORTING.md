Porting SyncAdapter to server Your Project
==========================================

0) change the package name
 -> mv  src/main/java/ca/kousu/contactsync/ src/main/java/your/package/path/
 -> change "package ca.kousu.contactsync" to "package your.package.path" in all .java files
 -> change "package="ca.kousu.contactsync"" to "package="your.package.path"" in AndroidManifest.xml
 -> change it in res/value/strings.xml
 -> change build.gradle:"applicationId 'ca.kousu.contactsync'" -> "you.package.path"
  TODO: look into how much of this is really necessary. maybe you can just change gradle and keep the rest the same?
 

1) change the icon
 -> just replace src/main/res/drawable/icon.png with your own
    check out https://icons8.com/ for ideas. They're freemium and free for open source!
    But you can search around for other sources, too, or find a designer, or whatever.
2) change the mapping logic
 edit ????.java 
 this part is the fiddliest. You have to write SQL-via-Java, and it's all Android-flavoured.
 Keep ... and ... open carefully while you're doing this.
 But don't worry, the outer structure.
