
/**
 * Copyright (C) 2017 nick@kousu.ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.kousu.contactsync_jmp;

import android.telephony.PhoneNumberUtils;

public class Util {
  public static String normalizePhoneNumber(String num) throws Exception { // TODO: pick a better exception. FormatException?
    String original_num = num;
    
    // this code is so dodgy
    num = PhoneNumberUtils.normalizeNumber(PhoneNumberUtils.extractNetworkPortion(num));

    // normalize to a 10 digit number; this essentially assumes north america. oops.
    if(num.length() == 12 && num.charAt(0) == '+') {
      num = num.substring(1); // clip off that dialing code
    }

    if(num.length() == 11 && num.charAt(0) == '1') { // 1 is the north american dialing code
      num = num.substring(1); // clip off that dialing code
    }
    
    if(num.length() != 10) {
      throw new Exception("'" + original_num + "' is not a North American number.");
    }

    return num;
  }
}
