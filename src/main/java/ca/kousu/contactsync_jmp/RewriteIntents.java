/**
 * Copyright (C) 2017 nick@kousu.ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.kousu.contactsync_jmp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;
import android.content.Context;
import android.util.Log;

import android.content.Intent;

import android.view.WindowManager;
import android.view.Window;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.provider.ContactsContract;
import android.content.ContentResolver;

import android.net.Uri;


public class RewriteIntents extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Intent intent = getIntent();
    //Log.d(this.getClass().getName(), intent.toString()); //DEBUG

    if(!intent.getScheme().equals("smsto")) {
      Log.e(this.getClass().getName(), "called with non-SMS intent. Check AndroidManifest.xml");
      finish();
      return;
    }

    // validate and reformat the number for Vitelity's format (which is 10 digits followed by @sms)
    // you'd think that PhoneNumberUtils.{normalizeNumber,stripSeparators}() would help, but they don't work as advertised
    // for example, normalizeNumber translates spaces to 0 and ( to 9s
    String num = intent.getData().getSchemeSpecificPart();
    String tidy_num;
    try {
      tidy_num = Util.normalizePhoneNumber(num);
    } catch(Exception e) {
      // bad format
      Toast.makeText(getApplicationContext(), e.getMessage() + " It is incompatible with Cheogram.", Toast.LENGTH_LONG).show();
      finish();
      return;
    }

    /// these two events seem to be effectively equivalent in every Android XMPP client; I don't know if one is preferred over the other, and I don't care.
    //Intent intent_ = new Intent(Intent.ACTION_SENDTO).setData(Uri.parse("imto://jabber" + "%2B1" + tidy_num + "@cheogram.com")); // '%28' = '+' in urlquoting
    Intent intent_ = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("xmpp:" + "+1" + tidy_num + "@cheogram.com"));
    //Log.d(this.getClass().getName(), intent_.toString()); // DEBUG
    startActivity(intent_);
    finish();
    return;
  }

}
