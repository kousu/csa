/**
 * Copyright (C) 2014 nick@kousu.ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.kousu.contactsync_jmp;

import java.util.List;
import java.util.ArrayList;

import android.app.Service;
import android.os.*;
import android.accounts.*;
import android.provider.*;
import android.content.*;
import android.database.*;
import android.net.Uri;

import android.util.Log;


/* TODO: add synchronization around the sync code
   it works fine and reliably single-threaded,
   but it seems like maybe if there's multiple syncs
   at the same time then it will do the thing
   where it loses the aggregation.

   That or maybe there's just a bug involving name changes.
 */

public class SyncAdapter extends Service {

  private static SyncAdapterImpl syncAdapter;

  /*
   * Start the contacts service.
   */
  @Override
  public synchronized void onCreate() {
    super.onCreate();

    Log.d(this.getClass().getName(), "onCreate()");
    syncAdapter = new SyncAdapterImpl(getApplicationContext(), true);


    // Trigger a sync whenever a phone number is edited
    // ((actually, trigger a resync whenever *anything* in the contacts list is edited, because
    // Android doesn't let us be more specific than that. So this is super inefficient. Suuuuper
    // super inefficient.))
    getApplicationContext().getContentResolver()
      .registerContentObserver(
      ContactsContract.CommonDataKinds.Phone.CONTENT_URI, // wat, this is triggering on content://com.android.contacts rather than anything more specific.
      /* WHAT?!
      > When false, the observer will be notified whenever a change occurs to the exact URI specified by uri or to one of the URI's ancestors in the path hierarchy. When true, the observer will also be notified whenever a change occurs to the URI's descendants in the path hierarchy.
      Read that closely: if you register for /a/b/c then you'll get notifications for /a and /a/b and there's no way(?) to turn it off.
      ..what?
      ..what?!
      omg android you are a .
      */
      false,
      new ContentObserver(null) {
       // Implement the onChange(boolean) method to delegate the change notification to
       // the onChange(boolean, Uri) method to ensure correct operation on older versions
       // of the framework that did not have the onChange(boolean, Uri) method.
       @Override
       public void onChange(boolean selfChange) {
           onChange(selfChange, null);
       }

       // Implement the onChange(boolean, Uri) method to take advantage of the new Uri argument.
       @Override
       public void onChange(boolean selfChange, Uri uri) {

        Account account = new Account(getApplicationContext().getString(R.string.app_name), getApplicationContext().getString(R.string.app_package));
        Log.d(SyncAdapter.class.getName(), "Phone database observer received change notice: (" + selfChange + ", " + uri.toString() + ")");
        // check if there if we have a sync in progress
        // this prevents an infinite forkbomb loop where we trigger our own updates
        if(getApplicationContext().getContentResolver().isSyncPending(account, ContactsContract.AUTHORITY)
           || getApplicationContext().getContentResolver().isSyncActive(account, ContactsContract.AUTHORITY)) return;

        if(!selfChange) {
          Log.d(SyncAdapter.class.getName(), "triggering resync.");

          Bundle bundle = new Bundle();
          bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true); // IMMEDIATE resync
          bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
          getApplicationContext().getContentResolver().requestSync(account, ContactsContract.AUTHORITY, bundle);
        }
       }

      });
  }

  @Override
  public IBinder onBind(Intent intent) {
    Log.d(this.getClass().getName(), "onBind()");
    // this has to return a SyncAdapter which actually does the work;
    // solve everything with more indirection, right Google?
    return syncAdapter.getSyncAdapterBinder();
  }

}



/* The "actual" sync adapter.

   Notice: here we use the *same logging tag* as the Service by choice
   because I really think these two classes should be one whole thing
   and Android is stupid and overbuilt.
 */
class SyncAdapterImpl extends AbstractThreadedSyncAdapter {

  public SyncAdapterImpl(Context context, boolean autoInitialize) {
    super(context, autoInitialize);

  }

  @Override
  public void onPerformSync(Account account, Bundle extras, String authority,
                            ContentProviderClient contentProviderClient, SyncResult syncResult) {
    Log.d(this.getClass().getName(), "onPerformSync(" + account.toString() + ", " + extras.toString() + ", " + authority.toString() + ", ..., ...)");

    /* TODO: handle cancellation
    > A sync is cancelled by issuing a interrupt() on the syncing thread. Either your code in
    > onPerformSync(Account, Bundle, String, ContentProviderClient, SyncResult) must check
    > interrupted(), or you you must override one of onSyncCanceled(Thread)/onSyncCanceled()
    > (depending on whether or not your adapter supports syncing of multiple accounts in parallel).
    > If your adapter does not respect the cancel issued by the framework you run the risk of your
    > app's entire process being killed.
    - https://developer.android.com/reference/android/content/AbstractThreadedSyncAdapter.html
    
    Since we're not talking to the network this is ~probably~ not a risk for us.
    */

    /* TODO: is contentProviderClient == getContext().getContentResolver() ? */

    try(Cursor contacts = getContext().getContentResolver()
                           .query(ContactsContract.Contacts.CONTENT_URI,
                                  new String[] {
                                    ContactsContract.Contacts._ID,
                                    ContactsContract.Contacts.DISPLAY_NAME,
                                    },
                                  null,
                                  null,
                                  null)) {
        while(contacts != null && contacts.moveToNext()) {
          Log.d(SyncAdapter.class.getName(), "Processing: Contact #" + contacts.getLong(0) + " = " + contacts.getString(1));

          long contact_id = contacts.getLong(0); // strongly typed you say, Java?
          String contact_name = contacts.getString(1);

          ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>(); // TODO: batch-ify this: move this list out of the loop and do a single batch at the end

          //super naive implementation: drop the RawContact for this account + unconditionally, which implicitly drops all Data rows
          // TODO: even more super naive: instead of filtering on LOOKUP_KEY, just do a single batch delete of all RawContacts from this account
          // CALLER_IS_SYNCADAPTER allows deletes to happen immediately, instead of setting the DELETED flag; maybe other things too?.
          operations.add(
            ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI
                                               .buildUpon()
                                               .appendQueryParameter(ContactsContract.RawContacts.ACCOUNT_NAME, account.name)
                                               .appendQueryParameter(ContactsContract.RawContacts.ACCOUNT_TYPE, account.type)
                                               .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true")
                                               .build())
              .withSelection(ContactsContract.RawContacts.CONTACT_ID + "=?", new String[]{String.valueOf(contact_id)})
              .build());
          
          // Create a new RawContact
          operations.add(
            ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
              .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, account.type)
              .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, account.name)
              .build());
         
          // the docs <https://developer.android.com/guide/topics/providers/contacts-provider.html#ContactBasics> are
          // basically non-existent (it feels intentional); the best I've found is a copy of the algorithm from an older
          // version of Android <https://android.stackexchange.com/questions/21709/how-does-android-contact-linking-work>.
          // (the docs just talk about "match an existing contact")
          // 
          // The basic way linking happens seems to be on StructuredName. If two RawContacts share a StructuredName then
          // they will get aggregated. And there's some fuzzy natural-language rules around what counts as "sharing".
          // You'd think that setting the foreign key RawContact.CONTACT_ID = Contact._ID would link them but noooo;
          // Apparently Android ignores you writing that and will rewrite the ID itself after reading the StructuredName.
          //
          // Another gotcha (at least on Android 4.4.4) is that if you insert two new RawContacts with the same name and
          // the same *Account* then they will all be split out, leaving you with *three* Contacts:
          // - the original
          // - the first RawContact (now wrapped in a newly generated Contact)
          // - the second RawContact
          //
          // I guess the design assumes there is a 1:1 between Accounts and RawContacts,
          // and if you want to attach more Data to a Contact then you need to stuff it all into that single RawContact.
          operations.add(
            ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
              .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 1) // backreference to the insert
              .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
              .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contact_name)
              .build());

          // loop over all phone numbers
          // morph the phone number to a Cheogram JID (<phonenumber>@cheogram.com)
          // then attach that to the associated contact (as a imto://jabber/ entry)
          try(Cursor numbers = getContext().getContentResolver()
                               .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                      new String[] {
                                        ContactsContract.RawContacts.CONTACT_ID, // the ID the super-meta Contact object
                                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                                        ContactsContract.Contacts.DISPLAY_NAME, // the ID the super-meta Contact object
                                        ContactsContract.Data._ID,               // the ID of this particular data row
                                        ContactsContract.Contacts.LOOKUP_KEY,
                                        },
                                      ContactsContract.RawContacts.CONTACT_ID + "=?", // <-- TODO: should I instead use ContactsContract.RawContacts.Entitires.CONTENT_URI? and filter that for phone numbers?
                                      new String[]{String.valueOf(contact_id)},
                                      null)) {
            while (numbers != null && numbers.moveToNext()) {
              Log.d(SyncAdapter.class.getName(),
                    "Considering phone number #" + numbers.getLong(3) + ": " + numbers.getString(1) + ", owned by contact #" + numbers.getLong(0) + "(#" + numbers.getLong(4) + ")" + " " + numbers.getString(2)
                   );


              String num = numbers.getString(1);
              String name = numbers.getString(2);
              String JID;
              {
                try {
                  JID = "+1" + Util.normalizePhoneNumber(num) + "@cheogram.com";
                } catch(Exception e) { // if normalizePhoneNumber() fails
                  Log.e(SyncAdapter.class.getName(), "Skipping " + num + ": " + e.toString());
                  continue;
                }
              }

              // this inserts the data we actually care about: the JID.
              // this whole DB feels like its trying really hard to be NoSQL..while definitely having all the pain of working with SQL.
              // what are you doing, Android?
              operations.add(
                ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                  .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 1)
                  .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)
                  .withValue(ContactsContract.CommonDataKinds.Im.TYPE, ContactsContract.CommonDataKinds.Im.TYPE_HOME)
                  .withValue(ContactsContract.CommonDataKinds.Im.PROTOCOL, ContactsContract.CommonDataKinds.Im.PROTOCOL_JABBER)
                  .withValue(ContactsContract.CommonDataKinds.Im.DATA, JID)
                  .build());
            } //while(numbers..)
          } catch(Exception e) { //try(Cursor numbers
            Log.e(SyncAdapter.class.getName(), e.toString());
          }

          if(!ContactsContract.AUTHORITY.equals(authority)) {
            throw new Exception("authority should be com.android.contacts");
          }
          // actually execute the operations: replace the old RawContact with the new RawContact
          // TODO: if we keep a counter, we can replace the valueBackReference with a real value and move this batch one loop outwards
          // which..might..matter?
          ContentProviderResult[] rs = getContext().getContentResolver().applyBatch(authority, operations);
          Log.d(SyncAdapter.class.getName(), "Showing " + rs.length + " applyBatch() results:");
          for(ContentProviderResult r : rs) {
            Log.d(SyncAdapter.class.getName(), "  ContentProviderResult[] = " + r.toString());
          } 
        } // while(contacts.. )
      } catch(Exception e) { // try(Cursor contacts
        Log.e(SyncAdapter.class.getName(), e.toString());
      }

    /* TODO: return something useful in syncResult
    - https://developer.android.com/reference/android/content/SyncResult.html
    */
  
  } // onPerformSync

} // class SyncAdapter


