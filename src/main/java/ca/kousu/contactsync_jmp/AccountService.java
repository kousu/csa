/**
 * Copyright (C) 2017 nick@kousu.ca
 * Copyright (C) 2014 Open Whisper Systems

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package ca.kousu.contactsync_jmp;

import android.os.*;
import android.accounts.*;
import android.app.Service;
import android.content.Context;
import android.provider.ContactsContract;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;


import android.widget.Toast;
import android.util.Log;


/* This is a simple no-op class which handles the "Add Account" dialog in the Android Settings menu, by responding by just closing entirely.
 * TODO: this might not be necessary at all; maybe I can add accounts to the account list without having to also add to the "Add Account" list.
         I think that would be better than a no-op maybe.
        But maybe we actually do want to handle this, e.g. configure *which* account the sync adapter is supposed to look at? or something? I dunno.
 */
public class AccountService extends Service {

  private static AccountAuthenticatorImpl accountAuthenticator = null;

  @Override
  public IBinder onBind(Intent intent) {
    if (intent.getAction().equals(android.accounts.AccountManager.ACTION_AUTHENTICATOR_INTENT)) {
      return getAuthenticator().getIBinder(); // why does a Service need to be another thing inside of itself and return that? What are you doing Android?!
    }
    return null;
  }

  private synchronized AccountAuthenticatorImpl getAuthenticator() {
    if (accountAuthenticator == null) {
      accountAuthenticator = new AccountAuthenticatorImpl(this);
    }

    return accountAuthenticator;
  }

  private static class AccountAuthenticatorImpl extends AbstractAccountAuthenticator { // why is this class static? 
    final Context context;

    public AccountAuthenticatorImpl(Context context) {
      super(context);
      this.context = context;
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType,
                             String[] requiredFeatures, Bundle options)
        throws NetworkErrorException
    {
      AccountManager accountManager = AccountManager.get(context);
      Account        account        = new Account(context.getString(R.string.app_name), accountType) ;

      Log.d(AccountService.class.getName(), "addAccount(" + "..." + ", " + accountType + ", " + authTokenType + ", ..., ...");

      if (accountManager.addAccountExplicitly(account, null, null)) {
        Log.i(AccountService.class.getName(), "Created new account " + account.toString());
        // enable syncing automatically (as a side effect, this will trigger an initial sync soon)
        context.getContentResolver().setIsSyncable(account, ContactsContract.AUTHORITY, 1);
        context.getContentResolver().setSyncAutomatically(account, ContactsContract.AUTHORITY, true);

        // I can't seem to send Toasts from in here which is annoying but okay android
        //Toast.makeText(context, context.getString(R.string.app_name) + " account created", Toast.LENGTH_LONG).show();
        // -> W/AccountAuthenticator(19395): java.lang.RuntimeException: Can't create handler inside thread that has not called Looper.prepare()
        // :/
        // The root cause is that this thing in here is running on a worker thread and Android isn't clever enough to
        // figure out how to get Toasts to automatically schedule themselves on the main thread. Good job Android.
        // https://stackoverflow.com/questions/3875184/cant-create-handler-inside-thread-that-has-not-called-looper-prepare
        // has three workarounds:
        // 1) the Android sanctioned way: create a Handler on the main thread and then send it a Message from the worker
        // 2) If you have an Activity lying around, wrap a lambda in Activity.runOnUiThread().
        // 3) Do basically the same as 1) with an AsyncTask
        // I've figured out how to improve on 1: you don't actually need to create the Handler on the main thread;
        // you can make it anywhere; Looper.getMainLooper() is the real magic sauce, and apparently that can be
        // called from anywhere. So, basically, this entire crufty thing below is just for the one inner line.
        // Bonus points: this entire crufty thing *closes over its scope*, so context.getString() means
        // AccountAuthenticatorImpl.context.getstring() :) :)
        (new Handler(Looper.getMainLooper()) {
          @Override
          public void handleMessage(Message message) {
            Toast.makeText(context, context.getString(R.string.app_name) + " account created.", Toast.LENGTH_LONG).show();
          }
        }).obtainMessage(0, null).sendToTarget();
      } else {
        // this can happen EITHER because the account already exists, or because we couldn't create it
        // which is kind of awkward: it means after this call we don't know if the account is there or not
        // and the only way I can tell to disambiguate it is by AccountManager.getAccounts() and looping through looking for yours.
        Log.e(AccountService.class.getName(), "Failed to create account!");
        (new Handler(Looper.getMainLooper()) {
          @Override
          public void handleMessage(Message message) {
            // TODO: be nicer to the user: *ask* if the account exists before trying to create it, maybe.
            // it looked like AccountManager will let us do this, but it'll make us fight it for it.
            Toast.makeText(context, "Couldn't create " + context.getString(R.string.app_name) + " account. Does it already exist?", Toast.LENGTH_LONG).show();
          }
        }).obtainMessage(0, null).sendToTarget();
      }

      // Tell Android that the account creation is finished.
      // Without this the phone won't know (what, the function finishing isn't enough?)
      // and and different ROMs will react differently, but generally all badly.
      //response.onResult(new Bundle());
      // or can we just:
      return new Bundle();

      // if in the future we find we need to extend to add some UI in (to e.g. ask for a domain name to tack onto the ends of things)
      // then we need to a) make an Intent to launch the UI Activity b) wrap that into inside of a Bundle, and the AccountAuthenticatorResponse
      // see e.g. https://gitlab.com/bitfireAT/icsdroid/blob/master/app/src/main/java/at/bitfire/icsdroid/AccountAuthenticatorService.java
    }

    /* provide dummy methods for the rest, since without these Java doesn't accept this class as non-abstract anymore */

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) {
      return null;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
      return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType,
                               Bundle options) throws NetworkErrorException {
      return null;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
      return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features)
        throws NetworkErrorException {
      return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType,
                                    Bundle options) {
      return null;
    }
  }
}
