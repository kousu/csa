Android Contacts Adapter
========================

This is an android [contacts provider sync adapter](https://developer.android.com/guide/topics/providers/contacts-provider.html#SyncAdapters)
which does the trick that Signal and WhatsApp do: mangle data from existing contact cards into clickable links.
 e.g. If under "IM" there's a "Facebook: xxxxx" entry, map it to https://facebook.com/xxxxx and/or xmpp:xxxxx@facebook-bridge.xmpp-example.com
 e.g. If under phone numbers there's +15155558888, map it to xmpp:5155558888@sms (for Vitelity) or xmpp:+15155558888@cheogram.com (for Soprani.ca/JMP)
(obviously my use case is XMPP-heavy, but you get the idea)

Rather than putting a lot of work in and making something hard to maintain, I am envisioning the code being extremely short, and having no UI.
Maybe an icon on the home screen just to make it easy to uninstall. But maybe not even that.
Instead of configuration, the code should be forked and modified; there will be a section of this document describing how to do those modifications once I've figured it out myself.


Links
-----

* [Android API reference](https://developer.android.com/reference/classes.html)
* [Android programming guide](https://developer.android.com/guide/index.html)
